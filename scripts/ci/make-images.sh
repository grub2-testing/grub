#!/bin/bash

set -eo pipefail

# Environment variables
# SRCDIR: Path to source files
# BUILDDIR: Directory in which to place the build
# TARGET: Target to test
# MAKE_ALL_IMAGE_TARGETS: If set to 'y', then all image targets, even disabled
#   ones, will be run.
# DISABLED_IMAGES: String of target formats to disable when building grub
#   images delimited by new lines.
# SHELL_TRACE: Set to 'y' to enable shell tracing
# NUM_FAILED_LOG_LINES: Set to integer number of log lines to display from the
#   end of a failed log file

if [ "x${SHELL_TRACE}" = "xy" ]; then
  # If we export SHELL_OPTS, then all shells will be traced, most of which we do not want
  SHELL_OPTS="-x"
  set -x
fi

[ -f "$(dirname "$0")/functions.sh" ] &&
. "$(dirname "$0")/functions.sh"

[ -f "$(dirname "$0")/functions.$CI_TYPE.sh" ] &&
. "$(dirname "$0")/functions.$CI_TYPE.sh"

start_log -c -n "images-$TARGET" "Making images for $TARGET"

if [ ! -x "${BUILDDIR}/grub-mkimage" ]; then
  echo "Aborting, no grub-mkimage found in builddir"
  exit 1
fi

function build_tformat() {
  TARGET=$1
  FORMATS=$2
  for fmt in $FORMATS; do
    if [ -z "${fmt%~*}" ]; then
      echo "${TARGET}"
    else
      echo "${TARGET}-${fmt}"
    fi
  done
}

tformats="~"
case "$TARGET" in
  mipsel-loongson)
    tformats="mipsel-loongson-elf mipsel-yeeloong-flash mipsel-fuloong2f-flash" ;;
  *)
    case "$TARGET" in
      i386-pc) tformats="~ pxe eltorito" ;;
      sparc64-ieee1275) tformats="aout cdcore raw" ;;
      mips-qemu_mips | \
      mipsel-qemu_mips) tformats="elf flash" ;;
      arm-coreboot) tformats="vexpress veyron" ;;
    esac
    tformats=$(build_tformat "${TARGET}" "$tformats")
    ;;
esac

RET=0
mkdir -pv "${BUILDDIR}/images"
for tfmt in $tformats; do
  if [ "x${MAKE_ALL_IMAGE_TARGETS}" != "xy" ] && ( echo "${DISABLED_IMAGES}" | grep -qE "^\s*${tfmt}$" ); then
    echo "Image build disabled for target format $tfmt"
    continue
  fi

  echo "Making image for target format: ${tfmt}"
  ${BUILDDIR}/grub-mkimage -v -p / -O "${tfmt}" -o "${BUILDDIR}/images/grub-${tfmt}.img" echo reboot normal > "${BUILDDIR}/grub-mkimage-${tfmt}.log" 2>&1 || _RET=$?
  [ "${_RET:-0}" -ne 0 ] && RET=$_RET

  if [ "$RET" -ne 0 ]; then
    echo -e "${TXT_RED}Failed to build image for target format ${tfmt}: ${TESTNAME}$TXT_CLEAR"
    echo -e -n "$TXT_LOG_COLOR"
    echo "Last ${NUM_FAILED_LOG_LINES} lines of grub-mkimage verbose log"
    tail -n "${NUM_FAILED_LOG_LINES}" "${BUILDDIR}/grub-mkimage-${tfmt}.log"
    echo -e -n "$TXT_CLEAR"
  fi
done

end_log -n "images-$TARGET"
exit $RET
