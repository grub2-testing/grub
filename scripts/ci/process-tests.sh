#!/bin/bash

set -eo pipefail

# Environment variables
# SRCDIR: Path to source files
# BUILDDIR: Directory in which to place the build
# TARGET: Target to process
# TEST_ALL_TARGETS: If set to 'y', then all tests, even disabled ones will be
#   processed.
# DISABLED_TESTS: String of disabled tests delimited by new lines.
# SHELL_TRACE: Set to 'y' to enable shell tracing
# EXPECTED_FAILURES: Multi-line string where each line contains a testname or
#   target:testname indicating an expected failure. Lines whose first non-space
#   character is '#' are ignored.
# EXPECTED_FUNCTIONAL_FAILURES: Same format as EXPECTED_FAILURES applied to the
#   functional test suite (grub_func_test).
# FAIL_ON_HARD_ERRORS: Set to 'y' to fail on hard as well as normal errors.
# IGNORE_TIMEDOUT_TEST_FAILURE: Set to 'y' to ignore test failures due to
#   test timing out.
# NUM_FAILED_LOG_LINES: Set to integer number of log lines to display from the
#   end of a failed log file

[ "x${SHELL_TRACE}" = "xy" ] && set -x

[ -f "$(dirname "$0")/functions.sh" ] &&
. "$(dirname "$0")/functions.sh"

[ -f "$(dirname "$0")/functions.$CI_TYPE.sh" ] &&
. "$(dirname "$0")/functions.$CI_TYPE.sh"

# TARGET="${ARCH}-${PLATFORM}"
NUM_FAILED_LOG_LINES=${NUM_FAILED_LOG_LINES:-100}

if [ -f ${BUILDDIR}/test.success ]; then
  echo "Not processing test logs because make check ran successfully"
  exit 0
elif [ "x${TEST_ALL_TARGETS}" != "xy" ] && ( echo "${DISABLED_TESTS}" | grep -qE "^\s*${TARGET}$" ); then
  echo "No test output processing because testing was disabled"
  exit 0
fi;

start_log -c -n "process-test-$TARGET" "Processing test output of $TARGET"
: >${BUILDDIR}/test-results.log
if [ -f ${BUILDDIR}/test-suite.log ]; then
  ( grep -E "^(FAIL|ERROR|SKIP):" ${BUILDDIR}/test-suite.log || ':' ) |
  while read STATUS TESTNAME; do
    STATUS=${STATUS%%:}

    if [ "$STATUS" = "SKIP" ]; then
      TYPE=skip
    elif tail -n1 "${BUILDDIR}/${TESTNAME}.log" | grep -qE 'exit status: (124|137)'; then
      if [ "x${IGNORE_TIMEDOUT_TEST_FAILURE}" = "xy" ]; then
        echo -e "${TXT_RED}Ignoring Timed-out test: ${TESTNAME}$TXT_CLEAR"
        echo -e -n "$TXT_LOG_COLOR"
        echo "Test failed due to a timeout. This is likely due to"
        echo "insufficient host resources, and NOT a real failure."
        echo -e -n "$TXT_CLEAR"
      fi
      TYPE=timeout
    elif [ "$STATUS" = "ERROR" ]; then
      TYPE=error
    elif echo "${EXPECTED_FAILURES}" | grep -qE "^\s*(${TESTNAME}|${TARGET}:${TESTNAME})$"; then
      echo "Expected failed test: $TESTNAME"
      TYPE=expected
    # If any unexpected failures in the functional tests, count a failure in
    # grub_func_test as a true failure.
    elif [ "$TESTNAME" = "grub_func_test" ]; then
      grep -E ': FAIL' ${BUILDDIR}/${TESTNAME}.log | sort -u |
      while read FTESTNAME STATUS; do
        FTESTNAME=${FTESTNAME%:*}
        if echo "${EXPECTED_FUNCTIONAL_FAILURES}" | grep -qE "^\s*(${FTESTNAME}|${TARGET}:${FTESTNAME})$"; then
          echo "Expected failed functional test: $FTESTNAME"
          TYPE=expected
        else
          echo -e "${TXT_RED}Unexpected failed functional test: ${FTESTNAME}$TXT_CLEAR"
          TYPE=unexpected
        fi
        echo "${TARGET}:${TYPE}:${TESTNAME}:${FTESTNAME}" >> ${BUILDDIR}/test-results.log
      done
      continue
    else
      echo -e "${TXT_RED}Unexpected failed test: ${TESTNAME}$TXT_CLEAR"
      echo -e -n "$TXT_LOG_COLOR"
      echo "Last ${NUM_FAILED_LOG_LINES} lines of ${BUILDDIR}/${TESTNAME}.log"
      tail -n "${NUM_FAILED_LOG_LINES}" "${BUILDDIR}/${TESTNAME}.log"
      echo -e -n "$TXT_CLEAR"
      TYPE=unexpected
    fi
    echo "${TARGET}:${TYPE}:${TESTNAME}" >> ${BUILDDIR}/test-results.log
  done
else
  echo "No success canary and no test-suite.log, perhaps tests were not run or was killed."
  RET=2
fi

# If there are any unexpected errors return exit with error
FAILCOND="unexpected"
if [ "x${FAIL_ON_HARD_ERRORS}" = "xy" ]; then
  FAILCOND="${FAILCOND}|error"
fi;
if [ "x${IGNORE_TIMEDOUT_TEST_FAILURE}" != "xy" ]; then
  FAILCOND="${FAILCOND}|timeout"
fi;
FAILCOND="${TARGET}:(${FAILCOND})"

if grep -E -qs "${FAILCOND}" ${BUILDDIR}/test-results.log; then
  RET=1
fi
end_log -n "process-test-$TARGET"
exit ${RET:-0}
