#!/bin/bash

set -eo pipefail

# Environment variables
# SRCDIR: Path to source files
# BUILDDIR: Directory in which to place the build
# TARGET: Target to test
# TEST_ALL_TARGETS: If set to 'y', then all tests, even disabled ones will be
#   run.
# DISABLED_TESTS: String of disabled tests delimited by new lines.
# SKIP_NATIVE_TESTS: If set to 'y', do not run native tests.
# TESTS_TIMEOUT: Maximum timeout to allow for 'make check'. Set this slightly
#   below total job timeout to allow for cleanup and log packaging code.
# TEST_VERBOSITY: Number to set verbosity level to
# TEST_DATA_PREFIX: Filename prefix to use when saving test data. If not set,
#   test data will not be saved.
# STRACE_TESTS: Set to 'y' to strace each test
# SHELL_TRACE: Set to 'y' to enable shell tracing
# JOBS: Number of concurrent jobs while building, defaults to number of
#   processors plus 1, unless number of processors is 1 in which case its 2.


if [ "x${SHELL_TRACE}" = "xy" ]; then
  # If we export SHELL_OPTS, then all shells will be traced, most of which we do not want
  SHELL_OPTS="-x"
  set -x
fi

[ -f "${0%/*}/functions.sh" ] &&
. "${0%/*}/functions.sh"

[ -f "${0%/*}/functions.$CI_TYPE.sh" ] &&
. "${0%/*}/functions.$CI_TYPE.sh"

if [ "x${TEST_ALL_TARGETS}" != "xy" ] && ( echo "${DISABLED_TESTS}" | grep -qE "^\s*${TARGET}$" ); then
  echo "Tests are disabled for target $TARGET"
  exit 0
fi

start_log -c -n "test-$TARGET" "Testing $TARGET"

if [ "${TEST_VERBOSITY:-0}" -gt 0 ]; then
  export GRUB_SHELL_DEFAULT_DEBUG=${TEST_VERBOSITY}
  export GRUB_TEST_DEFAULT_DEBUG=${TEST_VERBOSITY}
fi

JOBS="${JOBS:-`getconf _NPROCESSORS_ONLN 2> /dev/null || echo 2`}"
TESTTMPDIR="${TESTTMPDIR:-${TMPDIR:-/tmp}/grub-test-$TARGET}"
COMP=xz
STRACE_LOG="${BUILDDIR}/xxx.strace.$COMP"

if [ "x${STRACE_TESTS}" = "xy" ]; then
  STRACE="strace -y -yy -f -v -s4096 -o >($COMP -9 > $STRACE_LOG)"
fi

cat >${BUILDDIR}/log-tester.sh <<EOF
#!$SHELL $SHELL_OPTS
set -e

STRACE_LOG="$STRACE_LOG"
STRACE="$STRACE"
TESTNAME="\${1##*/}"
export SHELL_OPTS="$SHELL_OPTS"
export TMPDIR=$TESTTMPDIR/\$TESTNAME
mkdir -p "\$TMPDIR"

# if not a shell script, run normally
if [ "\$(head -c2 \$1)" = "#!" ]; then
  BUILD_SHEBANG="\$(head -n1 "${BUILDDIR}/grub-shell" | tail -c+3 | sed -E 's|^\\s*||')"
  TEST_SHELL=\$(head -n1 "\$1" | tail -c+3 | sed -E 's|^\\s*||')
  # Only turn on tracing if the shell is the one used by grub-shell
  if test "\${TEST_SHELL}" = "\${BUILD_SHEBANG}"; then
    if [ '(' "\${TEST_VERBOSITY:-0}" -gt 0 -o "x\${SHELL_TRACE}" = "xy" ')' \\
        -a '(' -z "\$SHELL_OPTS" -o -n "\${SHELL_OPTS##*-x*}" ')' ]; then
      SHELL_OPTS="\$SHELL_OPTS -x"
    fi
  fi

  # If script checks for root and GRUB_TEST_ROOTLESS is set, then hook
  if grep -q '"\$EUID" != 0' "\$1" && test -n "\$GRUB_TEST_ROOTLESS"; then
    TEST_SHELL="\$GRUB_TEST_ROOTLESS"
    export UML_ID="uml-grub-\${1##*/}"
  fi
fi

TSTART=\$(date +%s)
eval \$(echo -n "\$STRACE" | sed "s/xxx/\$TESTNAME/g") "\$TEST_SHELL" "\$@" || RET=\$?
TEND=\$(date +%s)
echo "Test duration in seconds: \$((TEND - TSTART))"

if [ "\${RET:-0}" -eq "0" ]; then
  rm -rf \$(echo -n "\$STRACE_LOG" | sed "s/xxx/\$TESTNAME/g") \$TMPDIR
elif [ "\${RET:-0}" -eq "124" ] || [ "\${RET:-0}" -eq "137" ]; then
  # This is these exitcodes are for a timed out process when using timeout
  # In this case return a hard error, otherwise it will be converted to a
  # normal error.
  exit 99
fi

exit \$RET
EOF
export LOG_COMPILER="${BUILDDIR}/log-tester.sh"
chmod +x ${BUILDDIR}/log-tester.sh
echo -n -e "${TXT_YELLOW}"
cat ${BUILDDIR}/log-tester.sh
echo -n -e "${TXT_CLEAR}"

# Setting debug to greater than 9 turns on the graphical qemu window.
# But we want to run in headless environments, so make sure debug values
# do not go above 9.
[ "${GRUB_SHELL_DEFAULT_DEBUG:-0}" -gt 9 ] && GRUB_SHELL_DEFAULT_DEBUG=9
[ "${GRUB_TEST_DEFAULT_DEBUG:-0}" -gt 9 ] && GRUB_TEST_DEFAULT_DEBUG=9

TIMEOUT=${TESTS_TIMEOUT}

RET=0
if [ "x${SKIP_NATIVE_TESTS}" = "xy" ]; then
  ${TIMEOUT:+timeout $TIMEOUT} make -C ${BUILDDIR} ${JOBS:+-j$JOBS} SUBDIRS= check-nonnative || RET=$?
else
  ${TIMEOUT:+timeout $TIMEOUT} make -C ${BUILDDIR} ${JOBS:+-j$JOBS} SUBDIRS= check || RET=$?
fi

if [ "$RET" -eq 0 ] && [ -d "$GRUB_PAYLOADS_DIR" ]; then
  ${TIMEOUT:+timeout $TIMEOUT} make -C ${BUILDDIR} ${JOBS:+-j$JOBS} SUBDIRS= bootcheck || RET=$?
fi
[ "$RET" -eq 124 -o "$RET" -eq 137 ] && echo "ERROR: make check timed out"

end_log -n "test-$TARGET"

[ "$RET" -eq 0 ] && touch ${BUILDDIR}/test.success || :
if [ -n "$TEST_DATA_PREFIX" ]; then
  tar -S -cJf "${TEST_DATA_PREFIX}.tar.xz" -C "${TESTTMPDIR%/*}" "${TESTTMPDIR##*/}"
fi

exit $RET
