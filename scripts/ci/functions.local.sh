#!/bin/bash

TXT_RED="\e[31m"
TXT_YELLOW="\e[33m"
TXT_CLEAR="\e[0m"
TXT_LOG_COLOR="\e[97;100m"

function start_log() {
  local LOG_COLLAPSE LOG_NAME
  while [ "$#" -gt 0 ]; do
    case "$1" in
      -c) LOG_COLLAPSE=1; shift;;
      -n) LOG_NAME="$2"; shift;;
      *) [ "$#" -eq 1 ] && LOG_MSG="$1"; shift;;
    esac
  done

  echo -e "=========="
  echo -e "========== ${TXT_YELLOW}Start Section ${LOG_NAME}: ${LOG_MSG}${TXT_CLEAR}"
  echo -e "=========="
  echo -e "${LOG_NAME} STARTTIME=`date +%s`" >&2
}

function end_log() {
  local LOG_NAME
  while [ "$#" -gt 0 ]; do
    case "$1" in
      -n) LOG_NAME=$2; shift;;
      *) shift;;
    esac
  done

  echo -e "${LOG_NAME} ENDTIME=`date +%s`" >&2
  echo -e "========== End Section ${LOG_NAME} =========="
}

:;
