#!/bin/bash

set -eo pipefail

# Environment variables
# JOBS: Number of concurrent jobs while building, defaults to number of
#   processors plus 1, unless number of processors is 1 in which case its 2.
# SRCDIR: Path to source files
# BUILDDIR: Directory in which to place the build
# INSTALLDIR: Directory to install binaries
# ARCH: Architecture to build for
# PLATFORM: Platform to build for
# CONFIGURE_OPTS: Extra configure options
# FONT_SOURCE: Path to unicode font named "unifont.<ext>" where ext is one of:
#   pcf pcf.gz bdf bdf.gz ttf ttf.gz
# DJVU_FONT_SOURCE: Path to DejaVu font named "DejaVuSans.<ext>" where ext is
#   one of: pcf pcf.gz bdf bdf.gz ttf ttf.gz
# SHELL_TRACE: Set to 'y' to enable shell tracing

[ "x${SHELL_TRACE}" = "xy" ] && set -x

[ -f "$(dirname "$0")/functions.sh" ] &&
. "$(dirname "$0")/functions.sh"

[ -f "$(dirname "$0")/functions.$CI_TYPE.sh" ] &&
. "$(dirname "$0")/functions.$CI_TYPE.sh"

JOBS=${JOBS:-`getconf _NPROCESSORS_ONLN 2> /dev/null || echo 2`}
[ "$JOBS" = 1 ] || JOBS=$(($JOBS + 1))

TARGET="${ARCH}-${PLATFORM}"

mkdir -pv ${BUILDDIR}

RET=0
cd ${BUILDDIR}

if test -f "$FONT_SOURCE"; then
  ln -svf "$(realpath -e "$FONT_SOURCE")" ./
fi

if test -f "$DJVU_FONT_SOURCE"; then
  ln -svf "$(realpath -e "$DJVU_FONT_SOURCE")" ./
fi

start_log -c -n "configure-$TARGET" "Configuring $TARGET"
[ -x "$SRCDIR/configure" ] && $SRCDIR/configure --help
show_build_env
$SRCDIR/configure --target=$ARCH --with-platform=$PLATFORM \
  --prefix=${INSTALLDIR} $CONFIGURE_OPTS || RET=$?
end_log -n "configure-$TARGET"

if [ "$RET" -ne 0 ]; then
  start_log -c -n "showlogs-$TARGET" "Configuring $TARGET failed, showing logs"
  cat ${BUILDDIR}/config.log
  end_log -n "showlogs-$TARGET"
  exit $RET
fi

start_log -c -n "build-$TARGET" "Building $TARGET"
show_build_env
make ${JOBS:+-j$JOBS} || RET=$?
end_log -n "build-$TARGET"
[ "$RET" -ne 0 ] && exit $RET

start_log -c -n "install-$TARGET" "Installing $TARGET"
make ${JOBS:+-j$JOBS} install || RET=$?
end_log -n "install-$TARGET"
exit $RET
