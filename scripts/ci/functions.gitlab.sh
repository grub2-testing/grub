#!/bin/bash

TXT_RED="\e[31m"
TXT_YELLOW="\e[33m"
TXT_CLEAR="\e[0m"
TXT_LOG_COLOR="\e[97;100m"

function start_log() {
  while [ "$#" -gt 0 ]; do
    case "$1" in
      -c) LOG_COLLAPSE=1; shift;;
      -n) LOG_NAME="$2"; shift;;
      *) [ "$#" -eq 1 ] && LOG_MSG="$1"; shift;;
    esac
  done

  echo -e "section_start:`date +%s`:${LOG_NAME}${LOG_COLLAPSE:+[collapsed=true]}\r\e[0K${LOG_MSG}"
}

function end_log() {
  while [ "$#" -gt 0 ]; do
    case "$1" in
      -n) LOG_NAME=$2; shift;;
      *) shift;;
    esac
  done

  echo -e "section_end:`date +%s`:${LOG_NAME}\r\e[0K"
}

:;
