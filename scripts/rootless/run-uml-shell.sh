#!/bin/sh
[ -z "$DEBUG" ] || set -x

SDIR=${0%/*}
ROOT=$(realpath -e "${ROOT:-"$SDIR/../../../grub-tests"}" 2>/dev/null)

if [ ! -d "$ROOT" ]; then
  echo "ROOT is not a directory or does not exists: ROOT=$ROOT" >&2
  exit 1
fi

export UML_ID=${UML_ID:-"uml-grub-shell-$$"}
if [ -d "$ROOT/rootfs.ovl" ]; then
  export UML_ROOT_OVERLAY=${UML_ROOT_OVERLAY:-"$ROOT/rootfs.ovl"}
fi

if [ -z "$UML_BIN" ]; then
  UML_BUILDDIR="$ROOT/build-uml"
  UML_KVER=${UML_KVER:-$(ls -1d "$UML_BUILDDIR/linux-5."*/|sort -rn|head -n1|sed -E 's|.*/linux-([^/]+)/$|\1|')}
  if [ -x "$UML_BUILDDIR/linux-$UML_KVER/linux" ]; then
    UML_BIN="$UML_BUILDDIR/linux-$UML_KVER/linux"
    if [ -d "$UML_BUILDDIR/linux-$UML_KVER/modules" ]; then
      UML_MODDIR="$UML_BUILDDIR/linux-$UML_KVER/modules"
    fi
  fi
  export UML_BIN UML_MODDIR
fi

if [ -z "$1" ]; then
  set -- $SHELL
fi

"$SDIR"/run-uml.sh "$@"
# This fixes the terminal after UML leaves it in a unusable state
reset
