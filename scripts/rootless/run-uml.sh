#!/bin/sh
set -e

[ -z "$DEBUG" ] || set -x
if [ "x$SHELL_TRACE" = "xy" ]; then
  set -x
fi

reshell() {
  if [ -n "$SHELL" ] && [ "$(realpath -e "$SHELL")" != "$(realpath -e /proc/$$/exe)" ] && \
     [ "$(head -n1 "$0" | tail -c+3 | sed -E 's|^\\s*||')" != "$SHELL" ]; then
    exec $SHELL $0 "$@"
  fi
}

ulimit -n 1024000
# In case these are not already in the PATH, we probably need them
PATH="$PATH:/sbin:/usr/sbin"
export PATH
CSEP=$(printf '\001')

set
if test -z "$RC"; then
  reshell "$@"
  if [ -z "$UML_BIN" ]; then
    if [ -x "/usr/bin/linux.uml" ]; then
      UML_BIN="/usr/bin/linux.uml"
      UML_MODDIR=${UML_MODDIR:-"/usr/lib/uml/modules"}
    else
      echo "UML_BIN not set and was not detected, ensure it is set to run under UML: $@" >&2
      exit 1
    fi
  fi
  UML_MEM=${UML_MEM:-64M}
  UML_MEM_MIN=256M
  if [ -z "$UML_MEM" ] || [ $(numfmt --from=iec --to-unit=1 "${UML_MEM}") -lt \
                            $(numfmt --from=iec --to-unit=1 "${UML_MEM_MIN}") ]; then
    echo "WARNING: Probably not enough memory for UML to run successfully, using ${UML_MEM}, likely need at least ${UML_MEM_MIN}" >&2
  fi
  UML_DEFAULTS=${UML_DEFAULTS:-"root=/dev/root rootfstype=hostfs rootflags=/ rw"}
  UML_ID=${UML_ID:-"uml-grub-$$"}
  UML_ROOT_OVERLAY=${UML_ROOT_OVERLAY:+$(realpath -e "$UML_ROOT_OVERLAY")}
  MODULES="minix,hfs,mac-roman,hfsplus,reiserfs,xfs,f2fs,btrfs,nilfs2,jfs,fat,ext4,udf,romfs,isofs,squashfs"
  MODULES="${MODULES},loop,fuse"

  get_envvars() {
    export | sed -E 's/^(declare -x|export) /export /' | grep -v ' BASH' | \
    grep -E " (CCACHE|DEBUG|GRUB|PATH|REAL_SHELL|SHELL|TMPDIR)" | grep -v "ROOTLESS"
  }

  # Unless one is explicitly given, try to find a good TMPDIR for UML, which
  # should be on tmpfs
  if [ -z "$UML_TMPDIR" ]; then
    for DIR in "$XDG_RUNTIME_DIR" "$TMPDIR" /dev/shm /tmp; do
      UML_TMPDIR="$DIR"
      [ "x$(stat -f -c %T "$UML_TMPDIR")" = 'xtmpfs' ] && [ -w "$UML_TMPDIR" ] && break
    done
  fi

  ENVVARFILE="$UML_TMPDIR/grub-uml-tester.env.$$"
  RCFILE="$UML_TMPDIR/grub-uml-tester.rc.$$"
  get_envvars >"$ENVVARFILE"

  # Set this after writing the envvar file so as not to overwrite TMPDIR inside UML
  export TMPDIR="$UML_TMPDIR"
  if [ "x$(stat -f -c %T "$TMPDIR")" != 'xtmpfs' ]; then
    echo "TMPDIR for UML should be on tmpfs, set UML_TMPDIR appropriately: UML_TMPDIR=$UML_TMPDIR" >&2
  fi

  if ! echo "$*" | grep -q "init="; then
    UML_INIT="init=$(realpath -e "$0")"
  fi

  CMDFILE="$TMPDIR/grub-uml-tester.cmd.$$"
  ( IFS=$CSEP; echo "$@" ) >"$CMDFILE"

  eval $GDB "$UML_BIN" ${UML_ID:+"umid=$UML_ID"} ${UML_DEFAULTS} \
       ${UML_MEM:+"mem=$UML_MEM"} "$UML_INIT" "CWD=\"$(pwd)\"" "RC=$RCFILE" \
       "MODDIR=$UML_MODDIR" "MODULES=$MODULES" "ROOTFS=${UML_ROOT_OVERLAY}" \
       "SHELL_TRACE=$SHELL_TRACE" "ENV=$ENVVARFILE" "CMDFILE=$CMDFILE" "$@"
  echo "UML_RET=$?"

  rm -f "$ENVVARFILE"
  exit "$(cat "$RCFILE"; rm -f "$RCFILE")"
fi

# Start UML init script
SDIR=$(dirname "$0")

if [ -n "$ENV" ]; then
  ENVDIR="${ENV%/*}"
  if [ ! -d "$ENVDIR" ]; then
    mkdir -p "$ENVDIR"
    mount -t hostfs -o"$ENVDIR" hostfs "$ENVDIR"
  fi
  . "$ENV"
fi

if [ -f "$CMDFILE" ]; then
  IFS_OLD=$IFS
  IFS=$CSEP
  V=$(cat "$CMDFILE")
  set -- $V
  IFS=$IFS_OLD
fi

shutdown() {
  # Need to call the reboot system call more directly because the poweroff,
  # halt, etc.. binaries are linked to systemd on the host and systemd is not
  # running here. So the binaries will complain and do nothing.
  # This constant is from man 2 reboot in case others are needed
  LINUX_REBOOT_CMD_POWER_OFF=0x4321fedc
  python3 -c "import ctypes; libc=ctypes.CDLL('libc.so.6'); libc.sync(); libc.reboot(${LINUX_REBOOT_CMD_POWER_OFF})"
}

RET=99
set_exit_code() {
  RET=$?
  if [ -n "$RC" ]; then
    echo -n "${RET:-0}" >"$RC"
    if [ "${RET:-0}" -ne "0" ]; then
      dmesg | tail
    fi
  fi
}

debug_shell() {
  if [ -n "$DEBUG" -a "${RET:-0}" != 0 ]; then
    echo "Error in script running debug shell..." >&2
    `command -v setsid` --ctty $SHELL -x -c "$CHROOT $SHELL" </dev/tty0 >/dev/tty0 2>&1
  fi
}
trap "set_exit_code; debug_shell; shutdown" EXIT INT QUIT KILL

UML_MODULES="overlay loop"

cd "$SDIR"

mount -t proc procfs /proc
reshell "$@"

mount -t sysfs sysfs /sys
mount -t tmpfs /media /media
mount -t tmpfs /tmp /tmp
mount -t tmpfs /run/mount /run/mount
OVERLAY_MNTDIR="/tmp/overlay"

mkdir -p /dev/pts
mount -t devpts -o noexec,nosuid,gid=5,mode=0620 devpts /dev/pts

mkdir -p /lib/modules
MODDIR=${MODDIR:-"$UML_MODDIR"}
if test -d "$MODDIR"; then
  mount --bind "$MODDIR" /lib/modules
fi

for MODULE in $UML_MODULES $(echo ${MODULES} | sed 's/,/ /g'); do
  modprobe $MODULE || :
done

SHELL=${SHELL:-$(head -n1 "$0" | cut -b3-)}

# lower kernel message verbosity to console
echo "3 4 1 3" >/proc/sys/kernel/printk

mkdir -p /media/oldroot
mount -o ro --rbind / /media/oldroot

OLDCWD=$(pwd)

if [ -n "$ROOTFS" ]; then
  # HACK: This is needed on some systems where /var/run is a symlink
  mkdir -p "$ROOTFS"/var/run

  mkdir -p /media/rootfs/lower1 /media/rootfs/lower2 /media/finalroot

  # Mount / in lower2 as an overlay and remove
  mount -t hostfs -o/ hostfs /media/rootfs/lower2
  mount -t hostfs -o"$ROOTFS" hostfs /media/rootfs/lower1

  OVERLAY_CONF="lowerdir=/media/rootfs/lower1:/media/rootfs/lower2"
  mount -o metacopy=on -t overlay overlay /media/finalroot -o"$OVERLAY_CONF" || { RET=$?; dmesg|tail; exit $RET; }

  mount --rbind "$OLDCWD" "/media/finalroot/$OLDCWD"

  for MNT in /dev /proc /run /sys /tmp /media /lib/modules; do
    mount --rbind "$MNT" /media/finalroot/"$MNT"
  done

  cd /media/finalroot
  pivot_root . /media/finalroot/media/oldroot
  cd "$OLDCWD"
fi

if [ -e "$TMPDIR" ]; then
  mount -t hostfs -o"$TMPDIR" hostfs-TMPDIR "$TMPDIR"
elif [ -n "$TMPDIR" ]; then
  mkdir -p "$TMPDIR"
fi

# HACK: Setup zfs-fuse
mount -t tmpfs /var/run /var/run
mkdir -p /var/run/lock
mount -t tmpfs /var/lock /var/lock
/etc/init.d/zfs-fuse start

# Ensure the RC directory goes to the host
mkdir -p "${RC%/*}"
mount -thostfs -o"${RC%/*}" "${RC%/*}" "${RC%/*}"

cd "$CWD"
if [ -n "$1" ]; then
  `command -v setsid` --ctty $SHELL -x -c "$CHROOT \"\$0\" \"\$@\"" "$@" </dev/tty0 >/dev/tty0 2>&1
  RET=$?
else
  `command -v setsid` --ctty $SHELL -x -c "$CHROOT $SHELL" </dev/tty0 >/dev/tty0 2>&1
fi

exit 0
