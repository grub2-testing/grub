#!/bin/sh
set -e
[ -z "$DEBUG" ] || set -x
[ "x$SHELL_TRACE" = "xy" ] && set -x

SDIR=$(realpath -e "${0%/*}")
# TODO: Make this a stable version once there is a stable on or above 5.14
DEF_KVER="5.14.8"
JOBS="${JOBS:-`getconf _NPROCESSORS_ONLN 2> /dev/null || echo 2`}"

# Need packages libncurses-dev bc

kernel_download() {
  local kver=$1
  local URLBASE="https://cdn.kernel.org/pub/linux/kernel"
  local URL="$URLBASE/v${kver%%.*}.x/linux-${kver}.tar.xz"

  if command -v curl >/dev/null 2>&1; then
    curl -C- -L -O "$URL"
  else
    wget -c "$URL"
  fi
}

kernel_unpack() {
  tar xJf "linux-$1.tar.xz"
}

main() {
  local kver="$1"

  if [ -z "$kver" ]; then
    kver="$DEF_KVER"
  fi

  if [ ! -d "linux-$kver" ]; then
    if [ ! -e "linux-$kver.tar.xz" ]; then
      kernel_download "$kver"
    fi
    kernel_unpack "$kver"
  fi

  export ARCH=um
  cd "linux-$kver"
  if [ -f "$SDIR"/uml-config.frag ]; then
    make defconfig
    ./scripts/kconfig/merge_config.sh .config "$SDIR"/uml-config.frag
  elif [ ! -f ".config" ]; then
    make menuconfig
  fi

  make ${JOBS:+-j$JOBS} &&
  make ${JOBS:+-j$JOBS} INSTALL_MOD_PATH=$(pwd)/modules.install modules_install
  [ -e modules ] || ln -s modules.install/lib/modules .
}

main "$@"
