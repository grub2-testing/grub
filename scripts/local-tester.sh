#!/bin/bash
set -e

SDIR=$(realpath -e "${SDIR:-"${0%/*}"}")
ROOT=$(realpath -m "${ROOT:-"./grub-tests"}")

mkdir -p "$ROOT"
cd "$ROOT"

# Log stdout+stderr to file while keeping stdout going to original stdout
exec 2>"$ROOT/$(basename "$0" .sh).$(date '+%Y%m%d%H%M%S').log" > >(exec 3>&1; exec tee >(exec cat >&3) >&2)
PTTY=$!

if [ "x$SHELL_TRACE" = "xy" ]; then
  set -x
  export >&2
fi

export CI_TYPE=local
if [ -d "${SDIR}/ci" ]; then
  export SCRIPTS_DIR=${SCRIPTS_DIR:-"${SDIR}"}
  export CI_SCRIPTS_DIR=${CI_SCRIPTS_DIR:-"${SCRIPTS_DIR}/ci"}
fi

# If found, source helper functions
[ -f "${CI_SCRIPTS_DIR}/functions.sh" ] &&
. "${CI_SCRIPTS_DIR}/functions.sh"


onexit() {
  set +x
  # Must close our end of the pipe to tee, else it will not exit and
  # we'll wait forever on it to exit.
  exec >&-
  wait_anypid $PTTY
}
trap onexit exit

main "$@"
